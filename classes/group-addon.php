<?php
/*
 * Handles classes
 * */
class Woocommerce_grouping_addon{
	
	const product_taxonomy = "product_cat";
	const product_posttype = "product";
	
	const meta_key_category = "_group_product_cat";
	const meta_key_size = "_group_product_number";
	const meta_key_discount = "_group_product_discount";
	const meta_key_des = "_group_product_des";
	const meta_key_max_number = '_group_product_max_number';
	
	static $is_checkout;
	
	//initialize the hooks
	static function init(){
		
		self::$is_checkout = false;
		
		add_action('init', array(get_class(), 'ainit'));
				
		//showing group products before every add to cart button
		add_action('woocommerce_before_add_to_cart_button', array(get_class(), 'woocommerce_before_add_to_cart_button'), 20);
		
		//validation during add and update
		add_filter('woocommerce_add_to_cart_validation', array(get_class(), 'validate_add_cart_item'), 10, 3);
		add_filter('woocommerce_update_cart_validation', array(get_class(), 'validate_update_cart_item'), 10, 4);
		
		//add to cart hooks
		add_filter('woocommerce_add_cart_item_data', array(get_class(), 'add_cart_item_data'), 20, 3);
		add_filter('woocommerce_add_cart_item', array(get_class(), 'add_cart_item'), 20, 1);
		
		
		//get product group info in cart
		add_filter('woocommerce_get_item_data', array(get_class(), 'get_item_data'), 15, 2);
		add_filter('woocommerce_get_cart_item_from_session', array(get_class(), 'get_cart_item_from_session'), 15, 2);
		
		
		//save group info with each order
		add_action('woocommerce_add_order_item_meta', array(get_class(), 'add_order_item_meta'), 20, 2);
		
		
		//stock controll
		add_action('woocommerce_checkout_order_processed', array(get_class(), 'order_processed'), 10, 2);
	//	add_action('woocommerce_reduce_order_stock', array(get_class(), 'reduce_stock'));
		//add_action('woocommerce_payment_complete', array(get_class(), 'reduce_stock'), 10, 1);
		
	
		add_action('woocommerce_check_cart_items', array(get_class(), 'woocommerce_check_cart_groups_items'));
		
		//add a write panel tab to supply settings
		add_action('woocommerce_product_write_panel_tabs', array(get_class(), 'woocommerce_product_write_panel_tabs'), 1000);
		add_filter('woocommerce_product_write_panels', array(get_class(), 'product_group_write_panels'));
		add_action('woocommerce_process_product_meta', array(get_class(), 'process_write_panels_meta'), 100, 2);
	
		//showing the order info with orders
		add_action('woocommerce_admin_order_item_headers', array(get_class(), 'admin_order_item_headers'));
		add_action('woocommerce_admin_order_item_values', array(get_class(), 'admin_order_item_values'), 100, 3);
	}
	
		
	
	//shows the group data
	static function woocommerce_before_add_to_cart_button(){
		global $post;
		
		if(self::is_product_group_enabled($post->ID)){
			include WOOCOMMERCE_PRODUCT_GROUPS_DIR . '/includes/before-add-to-cart-button.php';	
		}	
	}
	
	
	//returnt the post of a category
	static function get_products_by_category($term_id){
		
		//$term = get_term($term_id, self::product_taxonomy);
		
		$args = array(
			'post_type' => self::product_posttype,		
			
				'tax_query' => array(
						array(
						'taxonomy' => self::product_taxonomy,
						'field' => 'id',
						'terms' => $term_id
					 )
			),	
				
			'suppress_filters' => true,
			'nopaging' => true
		);	
		
		
		$query = new WP_Query($args);
			
		return $query;	
		
			
	}
	
	static function ainit(){
		//$query = self::get_products_by_category(14);
		//var_dump($query->posts); exit;	
	}
	
	
	//validate products while adding
	static function validate_add_cart_item($passed, $product_id, $qty){
		global $woocommerce;
		
		$total_quantity = 0;
		
		if(self::is_product_group_enabled($product_id) && isset($_POST['group-bundle-product']) && isset($_POST['group-bundle-product-quantity'])){
			foreach($_POST['group-bundle-product'] as $key => $bp){
				$bp_product = get_product($bp);
				
				$item_quantity = $qty * $_POST['group-bundle-product-quantity'][$key];
				$total_quantity += $item_quantity;
								
				if( $bp_product->managing_stock() && $bp_product->exists() && ($item_quantity > $bp_product->get_stock_quantity())){
					$passed = false;									
					$woocommerce->add_error("$item_quantity of {$bp_product->get_title()} can not be added. Only {$bp_product->get_stock_quantity()} is available");
					//return $passed;
				}
			}
		}		
		
		if(self::is_product_group_max_limit_enabled($product_id)){
			$metadata = self::get_product_groups_info($product_id);
			
			//var_dump($total_quantity);
			//var_dump($qty); var_dump($metadata['max_limit']); exit;
			
			if($total_quantity > absint($metadata['max_limit']) * $qty){
				$passed = false;
				$woocommerce->add_error("We only allow {$metadata['max_limit']} extra items with unit bundle");
				//return $passed;
			}
		}		
		
						
		return $passed;
	}
	
	
	//validate while updating
	static function validate_update_cart_item($passed, $cart_item_key, $values, $quantity){
		global $woocommerce;
		if(isset($values['group-bundle'])){
			$metadata = self::get_product_groups_info($product_id);
			$total_quantity = 0;
			
			$groups = array();
			foreach($values['group-bundle'] as $bundle){
				$groups[$bundle['product_id']][] = $bundle['name'];
			}
			
			foreach($groups as $key => $name){
				$item_quantity = count($name) * $quantity;
				
				$total_quantity += $item_quantity;
				$product = get_product($key);
				
				if($product->exists() && $product->managing_stock()){
					//checking product availability
					if(!$product->is_in_stock() || !$product->has_enough_stock($item_quantity)){
						$passed = false;
						
						$message = $product->get_title() . ' has only ' . $product->get_stock_quantity() . ' in stock. Please update the cart';
						$woocommerce->add_error($message);
					}
				}
				
			}
			
			if(self::is_product_group_max_limit_enabled($values['data']->id)){
				$metadata = self::get_product_groups_info($values['data']->id);
					
				if($total_quantity > absint($metadata['max_limit'] * $quantity)){
					$passed = false;
					$woocommerce->add_error("We only allow {$metadata['max_limit']} extra items with this bundle.");
				}
			}
			
		}
		
		return $passed;
	}
	
	
	

	//checking the cart group products
	static function woocommerce_check_cart_groups_items(){
		
		return self::check_cart_item_stock();
		
		global $woocommerce;
	
		// Check item stock
		$result =self::check_cart_item_stock();
	
		if (is_wp_error($result)){
			$woocommerce->add_error( $result->get_error_message() );
		}
	}
	
	
	
	/**
	 * Looks through the cart to check each item is in stock. If not, add an error.
	 *
	 * @access public
	 * @return bool
	 */
	static function check_cart_item_stock() {
			
		global $woocommerce;
				
		$total_quantity = 0;
		
		foreach($woocommerce->cart->cart_contents as $cart_item_key => $values){
	
			$groups = array();
			if(isset($values['group-bundle'])){
	
				foreach($values['group-bundle'] as $bundle){
					$groups[$bundle['product_id']][] = $bundle['name'];
				}
	
				//var_dump($groups);
	
				foreach($groups as $key => $name){
					$quantity = count($name) * $values['quantity'];
					$product = get_product($key);
					
					$total_quantity += $quantity;
					
					if($product->exists() && $product->managing_stock()){
						//checking product availability
						if(!$product->is_in_stock() || !$product->has_enough_stock($quantity)){
							$woocommerce->add_error($product->get_title() . ' has only ' . $product->get_stock_quantity() . ' in stock. But you are tryign to order ' . $quantity . '. Please update the cart.');
						}
					}
				}
			}
		}
		
		if(self::is_product_group_max_limit_enabled($values['data']->id)){
			$metadata = self::get_product_groups_info($values['data']->id);
			
			if($total_quantity > absint($metadata['max_limit']) * $values['quantity']){
				$woocommerce->add_error("We only allow {$metadata['max_limit']} extra items with unit bundle. Please update  your cart.");
			}
		}	
		
	}
	
	
	
	
		
	
	//add grouped cart items
	static function add_cart_item_data($cart_item_meta, $product_id, $variation_id){
		
		global $woocommerce;
		
		$cart_item_meta['group-bundle']= array();
			
		if(self::is_product_group_enabled($product_id) && isset($_POST['group-bundle-product']) && isset($_POST['group-bundle-product-quantity'])){
			
			//adjusting product price and discount
			$metadata = self::get_product_groups_info($product_id);
			$item = 0;
						
						
			foreach($_POST['group-bundle-product'] as $key => $bp){
				if($_POST['group-bundle-product-quantity'][$key] > 0){
										
					for($i=1; $i<=$_POST['group-bundle-product-quantity'][$key]; $i++){
						$item ++;					
						$bp_product = get_product($bp);
						
						$price = $bp_product->get_price();
						if($item <= (int)$metadata['product_number']){
							$discounted_price = 0;
							$status = 'free';
						}
						else{
							//$price = $price * (int) $_POST['group-bundle-product-quantity'][$key];
							$discounted_price = $price - $metadata['discount'];
							$status = woocommerce_price($metadata['discount']) . ' discounted';
						}
						$cart_item_meta['group-bundle'][] = array('name'=>$bp_product->get_title(), 'price'=>$price, 'new_price'=>$discounted_price, 'status' => $status, 'product_id'=>$bp_product->id);
					}
				}
			}
			
		}
			
	
		return $cart_item_meta;
	}
	
		
	
	//add cart itme
	static function add_cart_item($cart_item){
				
		if(isset($cart_item['group-bundle'])){
			$extra_cost = 0;
			
			$metadata = self::get_product_groups_info($cart_item['product_id']);
			$item = 0;
			
			foreach($cart_item['group-bundle'] as $b){
				
				$item ++;
				if($item <= (int)$metadata['product_number'] || empty($b['price'])) continue;
				
				$price = $b['price'] - $metadata['discount'];				
				$extra_cost += $price;
									
			}
							
			$cart_item['data']->adjust_price( $extra_cost );
			
		}	

				
		return $cart_item;
	}
	
	
	//return the item data with groped values
	static function get_item_data($other_data, $cart_item){
		if(isset($cart_item['group-bundle'])):
			foreach ($cart_item['group-bundle'] as $pr){
				//var_dump($pr);
				$other_data[] = array(
					'name' => $pr['name'],
					'price' => $pr['price'],
					'display' => woocommerce_price($pr['new_price']) . ' (' . $pr['status'] .')'
				);
			}
		endif;
		
		//var_dump($cart_item['group-bundle']);
		
		return $other_data;
	}
	
	
	//get cart items from session
	static function get_cart_item_from_session($cart_item, $values){
		
		
		if(isset($values['group-bundle'])){
			$cart_item['group-bundle'] = $values['group-bundle'];
			$cart_item = self::add_cart_item( $cart_item );
		}
				
		return $cart_item;
	}
	
	
	
	//order the meta items added with each product
	static function add_order_item_meta($item_id, $values){
		//var_dump($values['group-bundle']);
		//var_dump($item_id);
		if(isset($values['group-bundle'])){
			woocommerce_add_order_item_meta($item_id, '_group-bundle', serialize($values['group-bundle']), true);
		}
	}
		
	
	//proecessed order and reduce the stock 
	static function order_processed($order_id, $posted){
		self::$is_checkout = true;
		
		//return self::reduce_stock(new WC_Order($order_id));
	}
	
	
	//reduce stock with mother items
	static function reduce_stock($order){
		
		if(self::$is_checkout){
			if ( sizeof( $order->get_items() ) > 0 ) {
				foreach ( $order->get_items() as $item_id => $item ) {
				
					if ($item['product_id'] >0 ) {
						
						$group_bundle = woocommerce_get_order_item_meta($item_id, '_group-bundle', true);
						$group_bundle = empty($group_bundle) ? array() : unserialize($group_bundle);											
						
						if(count($group_bundle) > 0):
							
							foreach($group_bundle as $gb){							
								
								$_product = get_product( $gb['product_id'] );
						
								if ( $_product && $_product->exists() && $_product->managing_stock()) {	
																		
									$qty = apply_filters( 'woocommerce_order_item_quantity', $item['qty'], $this, $item );						
									$_product->reduce_stock( $qty );						
								}
							}
						endif;
				
					}
				
				}
				
			}
		}		
		
	}


	
	//write panel to add a new panel
	static function woocommerce_product_write_panel_tabs(){
		echo '<li class="product_group_management_options"><a href="#special_group_product_data"> Product Groups </a></li>';
	}
	
	static function product_group_write_panels(){
		include WOOCOMMERCE_PRODUCT_GROUPS_DIR . '/includes/write-panels/write-panel-product-group.php';
	}
	
	//save write panels info
	static function process_write_panels_meta($post_id, $post){
		if(isset($_POST['_product_group_status'])){
			self::set_product_group_status($post_id, 'yes');
		}
		else{
			self::set_product_group_status($post_id, 'no');
		}
		
		//var_dump($_POST['_product_group_limit_status']); exit;
		
		if(isset($_POST['_product_group_limit_status'])){
			self::set_product_group_limit_status($post_id, 'yes');
		}
		else{
			self::set_product_group_limit_status($post_id, 'no');
		}
		
		update_post_meta($post->ID, self::meta_key_category, $_POST[self::meta_key_category]);
		update_post_meta($post->ID, self::meta_key_discount, $_POST[self::meta_key_discount]);
		update_post_meta($post->ID, self::meta_key_size, $_POST[self::meta_key_size]);
		update_post_meta($post->ID, self::meta_key_des, $_POST[self::meta_key_des]);
		update_post_meta($post->ID, self::meta_key_max_number, $_POST[self::meta_key_max_number]);
	}
	
	//set product group status
	static function set_product_group_status($post_id, $status){
		update_post_meta($post_id, '_product_group_status', $status);
	}
	
	//bolean to check if product group is enabled
	static function is_product_group_enabled($post_id){
		return get_post_meta($post_id, '_product_group_status', true) == 'yes' ? true : false;
	}

	
	//product group limit status
	static function set_product_group_limit_status($post_id, $status){
		update_post_meta($post_id, '_product_group_limit_status', $status);
	}
	
	//boolean to check if the limit is appliable
	static function is_product_group_max_limit_enabled($post_id){
		return get_post_meta($post_id, '_product_group_limit_status', true) == 'yes' ? true : false;
	}
	
	
	//return the product terms
	static function get_product_terms(){
		return get_terms(self::product_taxonomy);
	}
	
	
	//return the product group info
	static function get_product_groups_info($post_id){
		return array(
				'term' => get_post_meta($post_id, self::meta_key_category, true),
				'discount' => get_post_meta($post_id, self::meta_key_discount, true),
				'product_number' => get_post_meta($post_id, self::meta_key_size, true),
				'description' => get_post_meta($post_id, self::meta_key_des, true),
				'max_limit' => get_post_meta($post_id, self::meta_key_max_number, true)
		);
	}
	
	/*
	 * return to the boolean functoin
	* still availabe for backward compatible
	* */
	static function is_group_mother($product_id){
		return self::is_product_group_enabled($product_id);
		//backword compatible
		$group_infos = self::get_product_groups_info($product_id);
		return (empty($group_infos)) ? false : true;
	}
	
	//show the product group in order details page
	static function admin_order_item_values($_product, $item, $item_id){
		$group_bundle = woocommerce_get_order_item_meta($item_id, '_group-bundle', true);
		$group_bundle = empty($group_bundle) ? array() : unserialize($group_bundle);
		
		$sanitized = array();
		if($group_bundle){
			
			foreach($group_bundle as $bundle){
				$sanitized[$bundle['product_id']][] = $bundle['name'];
			}
						
			include WOOCOMMERCE_PRODUCT_GROUPS_DIR . '/includes/write-panels/product_group_html.php';
		}
		
	}
	
	
	//add header
	static function admin_order_item_headers(){
		echo '<th class="item" colspan="2">Bundle Items</th>';
	}
	
}
