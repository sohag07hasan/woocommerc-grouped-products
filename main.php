<?php 
/*
 * plugin name: Woocommerce addons to create product groups
 * author: Mahibul Hasan Sohag
 * author uri: http://sohag07hasan.elance.com
 * Description: Woocommerce addons to make groups with the products. and handle the cart page, payment menthod professionally
 * Version: 1.1.2
 * Comatible: Woocommerce 2.14 tested
 */

define("WOOCOMMERCE_PRODUCT_GROUPS_DIR", dirname(__FILE__));
define("WOOCOMMERCE_PRODUCT_GROUPS_FILE", __FILE__);

include WOOCOMMERCE_PRODUCT_GROUPS_DIR . '/classes/group-addon.php';
Woocommerce_grouping_addon::init();
