<?php 
	
	$group_infos = self::get_product_groups_info($post->ID);
	
	if(empty($group_infos['term'])) return;
	
	$bundleproducts = self::get_products_by_category($group_infos['term']);
	
?>

<div class="productgroupwrap">
	<?php 
	global $blog_id;
	if ($blog_id == 3){
	?>
		<h2 id="selectcolours">Choisissez Vos Couleurs</h2>
	<?php
	}
	else{
	?>
		<h2 id="selectcolours">Select your colours</h2>
	<?php
	}
?>
<div class="product-group">
	<p class="product-group-description"> <?php echo $group_infos['description'];?> </p>
	<ul>
		<?php 
			
			if($bundleproducts->have_posts()){
				while($bundleproducts->have_posts()){
					$bundleproducts->the_post();
					$current_value = 0;
					
					$product = get_product(get_the_ID());
					//var_dump($product); die();
					if($product->is_in_stock()){
						//echo '<p><input '.checked(1, $current_value, false).' id="gropud-addon-'.$product->id.'" type="checkbox" name="group-bundle-product[]" value="'.$product->id.'"><label for="gropud-addon-'.$product->id.'">'.$post->post_title.'</label></p>';
						?>
							<li>
							<input type="hidden" name="group-bundle-product[]" value="<?php echo $product->id; ?>" />							
							
							<div class="quantity buttons_added">
								<input class="minus" type="button" value="-">
								<input readonly="readonly" id="id="gropud-numb-<?php echo $product->id; ?>"" class="input-text qty text" maxlength="12" title="Qty" size="4" value="0" data-max="<?php echo $product->get_stock_quantity(); ?>" data-min="0" name="group-bundle-product-quantity[]" />
								<input class="plus" type="button" value="+">
							</div>
							<label for="gropud-numb-<?php echo $product->id; ?>" class="colour-<?php echo $product->id; ?>"><?php echo $product->get_title(); ?></label>
							
							</li>
						<?php 
					}
				
				}			
			}
		?>
	
	</ul>
	<div class="clear"></div>	
</div>
</div><!-- end prod group wrap-->
<?php 
	global $blog_id;
	if ($blog_id == 3){
	?>
		<h2 class="step3">Ajoutez au panier</h2>
		<p>Ajoutez votre produit au panier et ensuite verifiez votre commande.</p>
	<?php
	}
	else{
	?>
		<h2 class="step3">Add to cart</h2>
		<p>Add your product to the cart and then proceed to the checkout page.</p>
	<?php
	}
?>

<?php wp_reset_query(); ?>
