<?php 
	
	global $post;
	
	$group_info = self::get_product_groups_info($post->ID);
	$product_cats = self::get_product_terms();
	$categories = array();
	foreach($product_cats as $cat){
		$categories[$cat->term_id] = $cat->name;
	}
	
	$enbaled = self::is_product_group_enabled($post->ID) ? 'yes' : 'no';
	$limit_enabled = self::is_product_group_max_limit_enabled($post->ID) ? 'yes' : 'no'; 
	
	
?>
<div class="panel woocommerce_options_panel" id="special_group_product_data">
	<div class="options_group">
		<?php woocommerce_wp_checkbox( array( 'id' => '_product_group_status', 'label' => __( 'Enable Product Group', 'woocommerce' ), 'cbvalue' => 'yes', 'value' => $enbaled, 'desc_tip' => true, 'description' => 'Make this product a product group' ) ); ?>
	</div>
	
	<div class="options_group">
		<?php woocommerce_wp_select( array( 'id' => self::meta_key_category, 'label' => __( 'Product Category', 'woocommerce' ), 'value' => $group_info['term'], 'options' => $categories, 'desc_tip' => true, 'description' => __( 'Products form this category will become items.', 'woocommerce' ) ) ); ?>
		<?php woocommerce_wp_text_input(  array( 'id' => self::meta_key_size, 'label' => __( 'Additional Free Items Quantity', 'woocommerce' ), 'description' => __( 'This quantity is free', 'woocommerce' ), 'value' => intval( $group_info['product_number'] ), 'type' => 'number', 'custom_attributes' => array('step'	=> '1') ) ); ?>
		<?php woocommerce_wp_text_input(  array( 'id' => self::meta_key_discount, 'label' => __( 'Discount Amount for each extra item', 'woocommerce' ), 'description' => __( 'Only applies if ordered quantity exceeds free quanity', 'woocommerce' ), 'value' => intval( $group_info['discount'] ), 'type' => 'number', 'custom_attributes' => array('step'	=> '1') ) ); ?>
		
		<?php woocommerce_wp_textarea_input(  array( 'id' => self::meta_key_des, 'label' => __( 'Description', 'woocommerce' ), 'description' => __( 'displayed above products, but within the grey box.', 'woocommerce' ) ) ); ?>
	</div>
	
	<div class="options_group">
		<?php woocommerce_wp_checkbox( array( 'id' => '_product_group_limit_status', 'label' => __( 'Set a limit ?', 'woocommerce' ), 'cbvalue' => 'yes', 'value' => $limit_enabled , 'desc_tip' => true, 'description' => 'Set a maximum limit for extra items' ) ); ?>
		<?php woocommerce_wp_text_input(  array( 'id' => self::meta_key_max_number, 'label' => __( 'Max Quantity Limit', 'woocommerce' ), 'description' => __( 'Customers can not choose extra items more than the maximum limit', 'woocommerce' ), 'value' => intval( $group_info['max_limit'] ), 'type' => 'number', 'custom_attributes' => array('step'	=> '1') ) ); ?>
	</div>
	
</div>