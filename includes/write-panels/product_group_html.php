<td>

<table>

<tr>
	<td>Items</td>
	<td>Qty</td>
</tr>

<?php 
foreach($sanitized as $p_id => $p){
$_product = get_product($p_id);
?>
	<tr>
		<td>
			<a target="_blank" href="<?php echo esc_url( admin_url( 'post.php?post='. absint( $_product->id ) .'&action=edit' ) ); ?>">
				<?php echo esc_html( $_product->get_title() ); ?>
			</a>
		</td>
		<td>
			<?php echo count($p); ?>
		</td>
		
	</tr>
<?php 
}
?>
</table>

</td>